<?php
Route::get('/welcome', function(){
    return view('welcome');
});
Route::get('/offline', function(){
    return view('modules/laravelpwa/offline');
});
Route::redirect('/', '/login');

Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/parse-csv-import', 'UsersController@parseCsvImport')->name('users.parseCsvImport');
    Route::post('users/process-csv-import', 'UsersController@processCsvImport')->name('users.processCsvImport');
    Route::resource('users', 'UsersController');

    // Categoria
    Route::delete('categoria/destroy', 'CategoriaController@massDestroy')->name('categoria.massDestroy');
    Route::post('categoria/media', 'CategoriaController@storeMedia')->name('categoria.storeMedia');
    Route::post('categoria/ckmedia', 'CategoriaController@storeCKEditorImages')->name('categoria.storeCKEditorImages');
    Route::post('categoria/parse-csv-import', 'CategoriaController@parseCsvImport')->name('categoria.parseCsvImport');
    Route::post('categoria/process-csv-import', 'CategoriaController@processCsvImport')->name('categoria.processCsvImport');
    Route::resource('categoria', 'CategoriaController');

    // Clasificacions
    Route::delete('clasificacions/destroy', 'ClasificacionController@massDestroy')->name('clasificacions.massDestroy');
    Route::post('clasificacions/media', 'ClasificacionController@storeMedia')->name('clasificacions.storeMedia');
    Route::post('clasificacions/ckmedia', 'ClasificacionController@storeCKEditorImages')->name('clasificacions.storeCKEditorImages');
    Route::post('clasificacions/parse-csv-import', 'ClasificacionController@parseCsvImport')->name('clasificacions.parseCsvImport');
    Route::post('clasificacions/process-csv-import', 'ClasificacionController@processCsvImport')->name('clasificacions.processCsvImport');
    Route::resource('clasificacions', 'ClasificacionController');

    // Autors
    Route::delete('autors/destroy', 'AutorController@massDestroy')->name('autors.massDestroy');
    Route::post('autors/parse-csv-import', 'AutorController@parseCsvImport')->name('autors.parseCsvImport');
    Route::post('autors/process-csv-import', 'AutorController@processCsvImport')->name('autors.processCsvImport');
    Route::resource('autors', 'AutorController');

    // Peliculas
    Route::delete('peliculas/destroy', 'PeliculasController@massDestroy')->name('peliculas.massDestroy');
    Route::post('peliculas/parse-csv-import', 'PeliculasController@parseCsvImport')->name('peliculas.parseCsvImport');
    Route::post('peliculas/process-csv-import', 'PeliculasController@processCsvImport')->name('peliculas.processCsvImport');
    Route::resource('peliculas', 'PeliculasController');

    // Boletos
    Route::delete('boletos/destroy', 'BoletosController@massDestroy')->name('boletos.massDestroy');
    Route::post('boletos/parse-csv-import', 'BoletosController@parseCsvImport')->name('boletos.parseCsvImport');
    Route::post('boletos/process-csv-import', 'BoletosController@processCsvImport')->name('boletos.processCsvImport');
    Route::resource('boletos', 'BoletosController');

    // Ctg Clientes
    Route::delete('ctg-clientes/destroy', 'CtgClienteController@massDestroy')->name('ctg-clientes.massDestroy');
    Route::post('ctg-clientes/media', 'CtgClienteController@storeMedia')->name('ctg-clientes.storeMedia');
    Route::post('ctg-clientes/ckmedia', 'CtgClienteController@storeCKEditorImages')->name('ctg-clientes.storeCKEditorImages');
    Route::post('ctg-clientes/parse-csv-import', 'CtgClienteController@parseCsvImport')->name('ctg-clientes.parseCsvImport');
    Route::post('ctg-clientes/process-csv-import', 'CtgClienteController@processCsvImport')->name('ctg-clientes.processCsvImport');
    Route::resource('ctg-clientes', 'CtgClienteController');

    // Clientecategoria
    Route::delete('clientecategoria/destroy', 'ClientecategoriaController@massDestroy')->name('clientecategoria.massDestroy');
    Route::post('clientecategoria/parse-csv-import', 'ClientecategoriaController@parseCsvImport')->name('clientecategoria.parseCsvImport');
    Route::post('clientecategoria/process-csv-import', 'ClientecategoriaController@processCsvImport')->name('clientecategoria.processCsvImport');
    Route::resource('clientecategoria', 'ClientecategoriaController');

    // Asientos
    Route::delete('asientos/destroy', 'AsientosController@massDestroy')->name('asientos.massDestroy');
    Route::post('asientos/parse-csv-import', 'AsientosController@parseCsvImport')->name('asientos.parseCsvImport');
    Route::post('asientos/process-csv-import', 'AsientosController@processCsvImport')->name('asientos.processCsvImport');
    Route::resource('asientos', 'AsientosController');

    // Salas
    Route::delete('salas/destroy', 'SalasController@massDestroy')->name('salas.massDestroy');
    Route::post('salas/parse-csv-import', 'SalasController@parseCsvImport')->name('salas.parseCsvImport');
    Route::post('salas/process-csv-import', 'SalasController@processCsvImport')->name('salas.processCsvImport');
    Route::resource('salas', 'SalasController');

    // Tickets
    Route::delete('tickets/destroy', 'TicketController@massDestroy')->name('tickets.massDestroy');
    Route::post('tickets/parse-csv-import', 'TicketController@parseCsvImport')->name('tickets.parseCsvImport');
    Route::post('tickets/process-csv-import', 'TicketController@processCsvImport')->name('tickets.processCsvImport');
    Route::resource('tickets', 'TicketController');

    // Teams
    Route::delete('teams/destroy', 'TeamController@massDestroy')->name('teams.massDestroy');
    Route::resource('teams', 'TeamController');

    Route::get('system-calendar', 'SystemCalendarController@index')->name('systemCalendar');
    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');
});
