<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Categoria
    Route::post('categoria/media', 'CategoriaApiController@storeMedia')->name('categoria.storeMedia');
    Route::apiResource('categoria', 'CategoriaApiController');

    // Clasificacions
    Route::post('clasificacions/media', 'ClasificacionApiController@storeMedia')->name('clasificacions.storeMedia');
    Route::apiResource('clasificacions', 'ClasificacionApiController');

    // Autors
    Route::apiResource('autors', 'AutorApiController');

    // Peliculas
    Route::apiResource('peliculas', 'PeliculasApiController');

    // Boletos
    Route::apiResource('boletos', 'BoletosApiController');

    // Ctg Clientes
    Route::post('ctg-clientes/media', 'CtgClienteApiController@storeMedia')->name('ctg-clientes.storeMedia');
    Route::apiResource('ctg-clientes', 'CtgClienteApiController');

    // Clientecategoria
    Route::apiResource('clientecategoria', 'ClientecategoriaApiController');

    // Asientos
    Route::apiResource('asientos', 'AsientosApiController');

    // Salas
    Route::apiResource('salas', 'SalasApiController');

    // Tickets
    Route::apiResource('tickets', 'TicketApiController');

    // Teams
    Route::apiResource('teams', 'TeamApiController');

});
