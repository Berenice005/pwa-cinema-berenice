<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asiento extends Model
{
    use SoftDeletes;

    public $table = 'asientos';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'sala_id',
        'id_asiento',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function sala()
    {
        return $this->belongsTo(Sala::class, 'sala_id');

    }
}
