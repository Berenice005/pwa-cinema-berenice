<?php

namespace App\Http\Requests;

use App\Clientecategorium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreClientecategoriumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('clientecategorium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_cat_user'  => [
                'required',
                'unique:clientecategoria'],
            'cliente_id'   => [
                'required',
                'integer'],
            'categoria_id' => [
                'required',
                'integer'],
        ];

    }
}
