<?php

namespace App\Http\Requests;

use App\Clasificacion;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreClasificacionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('clasificacion_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_clasificacion' => [
                'required',
                'unique:clasificacions'],
        ];

    }
}
