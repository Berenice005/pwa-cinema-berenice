<?php

namespace App\Http\Requests;

use App\CtgCliente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyCtgClienteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ctg_cliente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ctg_clientes,id',
        ];

    }
}
