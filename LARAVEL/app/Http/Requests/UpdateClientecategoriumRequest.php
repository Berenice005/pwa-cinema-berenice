<?php

namespace App\Http\Requests;

use App\Clientecategorium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateClientecategoriumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('clientecategorium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_cat_user'  => [
                'required',
                'unique:clientecategoria,id_cat_user,' . request()->route('clientecategorium')->id],
            'cliente_id'   => [
                'required',
                'integer'],
            'categoria_id' => [
                'required',
                'integer'],
        ];

    }
}
