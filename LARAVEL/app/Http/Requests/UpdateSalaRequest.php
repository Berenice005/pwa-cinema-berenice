<?php

namespace App\Http\Requests;

use App\Sala;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateSalaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('sala_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_sala' => [
                'required',
                'unique:salas,id_sala,' . request()->route('sala')->id],
        ];

    }
}
