<?php

namespace App\Http\Requests;

use App\Clientecategorium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyClientecategoriumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('clientecategorium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:clientecategoria,id',
        ];

    }
}
