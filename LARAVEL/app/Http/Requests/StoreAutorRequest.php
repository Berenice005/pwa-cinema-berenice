<?php

namespace App\Http\Requests;

use App\Autor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreAutorRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('autor_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_author' => [
                'required',
                'unique:autors'],
            'nombre'    => [
                'required'],
        ];

    }
}
