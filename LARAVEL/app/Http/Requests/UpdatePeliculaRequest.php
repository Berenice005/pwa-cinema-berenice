<?php

namespace App\Http\Requests;

use App\Pelicula;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdatePeliculaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('pelicula_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_pelicula'      => [
                'required',
                'unique:peliculas,id_pelicula,' . request()->route('pelicula')->id],
            'nombre'           => [
                'required',
                'unique:peliculas,nombre,' . request()->route('pelicula')->id],
            'autor_id'         => [
                'required',
                'integer'],
            'categoria_id'     => [
                'required',
                'integer'],
            'clasificacion_id' => [
                'required',
                'integer'],
        ];

    }
}
