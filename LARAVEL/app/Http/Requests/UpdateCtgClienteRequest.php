<?php

namespace App\Http\Requests;

use App\CtgCliente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateCtgClienteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ctg_cliente_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_categoria' => [
                'required',
                'unique:ctg_clientes,id_categoria,' . request()->route('ctg_cliente')->id],
        ];

    }
}
