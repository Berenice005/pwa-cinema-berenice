<?php

namespace App\Http\Requests;

use App\Pelicula;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StorePeliculaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('pelicula_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_pelicula'      => [
                'required',
                'unique:peliculas'],
            'nombre'           => [
                'required',
                'unique:peliculas'],
            'autor_id'         => [
                'required',
                'integer'],
            'categoria_id'     => [
                'required',
                'integer'],
            'clasificacion_id' => [
                'required',
                'integer'],
        ];

    }
}
