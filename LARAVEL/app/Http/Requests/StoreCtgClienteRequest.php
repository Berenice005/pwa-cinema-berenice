<?php

namespace App\Http\Requests;

use App\CtgCliente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreCtgClienteRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ctg_cliente_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_categoria' => [
                'required',
                'unique:ctg_clientes'],
        ];

    }
}
