<?php

namespace App\Http\Requests;

use App\Boleto;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateBoletoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('boleto_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_boleto'   => [
                'required',
                'unique:boletos,id_boleto,' . request()->route('boleto')->id],
            'pelicula_id' => [
                'required',
                'integer'],
            'precio'      => [
                'required'],
            'horario'     => [
                'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format')],
        ];

    }
}
