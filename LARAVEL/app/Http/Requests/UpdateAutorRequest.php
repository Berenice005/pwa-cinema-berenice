<?php

namespace App\Http\Requests;

use App\Autor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateAutorRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('autor_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_author' => [
                'required',
                'unique:autors,id_author,' . request()->route('autor')->id],
            'nombre'    => [
                'required'],
        ];

    }
}
