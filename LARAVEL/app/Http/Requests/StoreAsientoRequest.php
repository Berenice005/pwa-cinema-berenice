<?php

namespace App\Http\Requests;

use App\Asiento;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreAsientoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('asiento_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'id_asiento' => [
                'required',
                'unique:asientos'],
            'sala_id'    => [
                'required',
                'integer'],
        ];

    }
}
