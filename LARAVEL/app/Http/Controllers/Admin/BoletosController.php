<?php

namespace App\Http\Controllers\Admin;

use App\Boleto;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyBoletoRequest;
use App\Http\Requests\StoreBoletoRequest;
use App\Http\Requests\UpdateBoletoRequest;
use App\Pelicula;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class BoletosController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('boleto_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Boleto::with(['pelicula'])->select(sprintf('%s.*', (new Boleto)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'boleto_show';
                $editGate      = 'boleto_edit';
                $deleteGate    = 'boleto_delete';
                $crudRoutePart = 'boletos';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_boleto', function ($row) {
                return $row->id_boleto ? $row->id_boleto : "";
            });
            $table->addColumn('pelicula_nombre', function ($row) {
                return $row->pelicula ? $row->pelicula->nombre : '';
            });

            $table->editColumn('precio', function ($row) {
                return $row->precio ? $row->precio : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'pelicula']);

            return $table->make(true);
        }

        return view('admin.boletos.index');
    }

    public function create()
    {
        abort_if(Gate::denies('boleto_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $peliculas = Pelicula::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.boletos.create', compact('peliculas'));
    }

    public function store(StoreBoletoRequest $request)
    {
        $boleto = Boleto::create($request->all());

        return redirect()->route('admin.boletos.index');

    }

    public function edit(Boleto $boleto)
    {
        abort_if(Gate::denies('boleto_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $peliculas = Pelicula::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $boleto->load('pelicula');

        return view('admin.boletos.edit', compact('peliculas', 'boleto'));
    }

    public function update(UpdateBoletoRequest $request, Boleto $boleto)
    {
        $boleto->update($request->all());

        return redirect()->route('admin.boletos.index');

    }

    public function show(Boleto $boleto)
    {
        abort_if(Gate::denies('boleto_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boleto->load('pelicula', 'idBoletoTickets');

        return view('admin.boletos.show', compact('boleto'));
    }

    public function destroy(Boleto $boleto)
    {
        abort_if(Gate::denies('boleto_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boleto->delete();

        return back();

    }

    public function massDestroy(MassDestroyBoletoRequest $request)
    {
        Boleto::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
