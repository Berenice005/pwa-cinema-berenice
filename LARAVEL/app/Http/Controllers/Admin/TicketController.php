<?php

namespace App\Http\Controllers\Admin;

use App\Asiento;
use App\Boleto;
use App\Clientecategorium;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyTicketRequest;
use App\Http\Requests\StoreTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Ticket;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class TicketController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('ticket_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Ticket::with(['cliente', 'id_boleto', 'asiento'])->select(sprintf('%s.*', (new Ticket)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'ticket_show';
                $editGate      = 'ticket_edit';
                $deleteGate    = 'ticket_delete';
                $crudRoutePart = 'tickets';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_ticket', function ($row) {
                return $row->id_ticket ? $row->id_ticket : "";
            });
            $table->addColumn('cliente_id_cat_user', function ($row) {
                return $row->cliente ? $row->cliente->id_cat_user : '';
            });

            $table->editColumn('cliente.id_cat_user', function ($row) {
                return $row->cliente ? (is_string($row->cliente) ? $row->cliente : $row->cliente->id_cat_user) : '';
            });
            $table->addColumn('id_boleto_id_boleto', function ($row) {
                return $row->id_boleto ? $row->id_boleto->id_boleto : '';
            });

            $table->editColumn('id_boleto.precio', function ($row) {
                return $row->id_boleto ? (is_string($row->id_boleto) ? $row->id_boleto : $row->id_boleto->precio) : '';
            });
            $table->addColumn('asiento_id_asiento', function ($row) {
                return $row->asiento ? $row->asiento->id_asiento : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'cliente', 'id_boleto', 'asiento']);

            return $table->make(true);
        }

        return view('admin.tickets.index');
    }

    public function create()
    {
        abort_if(Gate::denies('ticket_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientes = Clientecategorium::all()->pluck('id_cat_user', 'id')->prepend(trans('global.pleaseSelect'), '');

        $id_boletos = Boleto::all()->pluck('id_boleto', 'id')->prepend(trans('global.pleaseSelect'), '');

        $asientos = Asiento::all()->pluck('id_asiento', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.tickets.create', compact('clientes', 'id_boletos', 'asientos'));
    }

    public function store(StoreTicketRequest $request)
    {
        $ticket = Ticket::create($request->all());

        return redirect()->route('admin.tickets.index');

    }

    public function edit(Ticket $ticket)
    {
        abort_if(Gate::denies('ticket_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientes = Clientecategorium::all()->pluck('id_cat_user', 'id')->prepend(trans('global.pleaseSelect'), '');

        $id_boletos = Boleto::all()->pluck('id_boleto', 'id')->prepend(trans('global.pleaseSelect'), '');

        $asientos = Asiento::all()->pluck('id_asiento', 'id')->prepend(trans('global.pleaseSelect'), '');

        $ticket->load('cliente', 'id_boleto', 'asiento');

        return view('admin.tickets.edit', compact('clientes', 'id_boletos', 'asientos', 'ticket'));
    }

    public function update(UpdateTicketRequest $request, Ticket $ticket)
    {
        $ticket->update($request->all());

        return redirect()->route('admin.tickets.index');

    }

    public function show(Ticket $ticket)
    {
        abort_if(Gate::denies('ticket_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ticket->load('cliente', 'id_boleto', 'asiento');

        return view('admin.tickets.show', compact('ticket'));
    }

    public function destroy(Ticket $ticket)
    {
        abort_if(Gate::denies('ticket_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ticket->delete();

        return back();

    }

    public function massDestroy(MassDestroyTicketRequest $request)
    {
        Ticket::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
