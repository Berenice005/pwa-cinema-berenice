<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroySalaRequest;
use App\Http\Requests\StoreSalaRequest;
use App\Http\Requests\UpdateSalaRequest;
use App\Sala;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class SalasController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('sala_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Sala::query()->select(sprintf('%s.*', (new Sala)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'sala_show';
                $editGate      = 'sala_edit';
                $deleteGate    = 'sala_delete';
                $crudRoutePart = 'salas';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_sala', function ($row) {
                return $row->id_sala ? $row->id_sala : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.salas.index');
    }

    public function create()
    {
        abort_if(Gate::denies('sala_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.salas.create');
    }

    public function store(StoreSalaRequest $request)
    {
        $sala = Sala::create($request->all());

        return redirect()->route('admin.salas.index');

    }

    public function edit(Sala $sala)
    {
        abort_if(Gate::denies('sala_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.salas.edit', compact('sala'));
    }

    public function update(UpdateSalaRequest $request, Sala $sala)
    {
        $sala->update($request->all());

        return redirect()->route('admin.salas.index');

    }

    public function show(Sala $sala)
    {
        abort_if(Gate::denies('sala_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.salas.show', compact('sala'));
    }

    public function destroy(Sala $sala)
    {
        abort_if(Gate::denies('sala_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sala->delete();

        return back();

    }

    public function massDestroy(MassDestroySalaRequest $request)
    {
        Sala::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
