<?php

namespace App\Http\Controllers\Admin;

use App\Asiento;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyAsientoRequest;
use App\Http\Requests\StoreAsientoRequest;
use App\Http\Requests\UpdateAsientoRequest;
use App\Sala;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AsientosController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('asiento_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Asiento::with(['sala'])->select(sprintf('%s.*', (new Asiento)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'asiento_show';
                $editGate      = 'asiento_edit';
                $deleteGate    = 'asiento_delete';
                $crudRoutePart = 'asientos';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_asiento', function ($row) {
                return $row->id_asiento ? $row->id_asiento : "";
            });
            $table->addColumn('sala_id_sala', function ($row) {
                return $row->sala ? $row->sala->id_sala : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'sala']);

            return $table->make(true);
        }

        return view('admin.asientos.index');
    }

    public function create()
    {
        abort_if(Gate::denies('asiento_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $salas = Sala::all()->pluck('id_sala', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.asientos.create', compact('salas'));
    }

    public function store(StoreAsientoRequest $request)
    {
        $asiento = Asiento::create($request->all());

        return redirect()->route('admin.asientos.index');

    }

    public function edit(Asiento $asiento)
    {
        abort_if(Gate::denies('asiento_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $salas = Sala::all()->pluck('id_sala', 'id')->prepend(trans('global.pleaseSelect'), '');

        $asiento->load('sala');

        return view('admin.asientos.edit', compact('salas', 'asiento'));
    }

    public function update(UpdateAsientoRequest $request, Asiento $asiento)
    {
        $asiento->update($request->all());

        return redirect()->route('admin.asientos.index');

    }

    public function show(Asiento $asiento)
    {
        abort_if(Gate::denies('asiento_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $asiento->load('sala');

        return view('admin.asientos.show', compact('asiento'));
    }

    public function destroy(Asiento $asiento)
    {
        abort_if(Gate::denies('asiento_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $asiento->delete();

        return back();

    }

    public function massDestroy(MassDestroyAsientoRequest $request)
    {
        Asiento::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
