<?php

namespace App\Http\Controllers\Admin;

use App\Autor;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyAutorRequest;
use App\Http\Requests\StoreAutorRequest;
use App\Http\Requests\UpdateAutorRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AutorController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('autor_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Autor::query()->select(sprintf('%s.*', (new Autor)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'autor_show';
                $editGate      = 'autor_edit';
                $deleteGate    = 'autor_delete';
                $crudRoutePart = 'autors';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_author', function ($row) {
                return $row->id_author ? $row->id_author : "";
            });
            $table->editColumn('nombre', function ($row) {
                return $row->nombre ? $row->nombre : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.autors.index');
    }

    public function create()
    {
        abort_if(Gate::denies('autor_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.autors.create');
    }

    public function store(StoreAutorRequest $request)
    {
        $autor = Autor::create($request->all());

        return redirect()->route('admin.autors.index');

    }

    public function edit(Autor $autor)
    {
        abort_if(Gate::denies('autor_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.autors.edit', compact('autor'));
    }

    public function update(UpdateAutorRequest $request, Autor $autor)
    {
        $autor->update($request->all());

        return redirect()->route('admin.autors.index');

    }

    public function show(Autor $autor)
    {
        abort_if(Gate::denies('autor_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.autors.show', compact('autor'));
    }

    public function destroy(Autor $autor)
    {
        abort_if(Gate::denies('autor_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $autor->delete();

        return back();

    }

    public function massDestroy(MassDestroyAutorRequest $request)
    {
        Autor::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
