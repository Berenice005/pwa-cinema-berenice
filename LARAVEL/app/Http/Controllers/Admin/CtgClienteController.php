<?php

namespace App\Http\Controllers\Admin;

use App\CtgCliente;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCtgClienteRequest;
use App\Http\Requests\StoreCtgClienteRequest;
use App\Http\Requests\UpdateCtgClienteRequest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class CtgClienteController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('ctg_cliente_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = CtgCliente::query()->select(sprintf('%s.*', (new CtgCliente)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'ctg_cliente_show';
                $editGate      = 'ctg_cliente_edit';
                $deleteGate    = 'ctg_cliente_delete';
                $crudRoutePart = 'ctg-clientes';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_categoria', function ($row) {
                return $row->id_categoria ? $row->id_categoria : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.ctgClientes.index');
    }

    public function create()
    {
        abort_if(Gate::denies('ctg_cliente_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.ctgClientes.create');
    }

    public function store(StoreCtgClienteRequest $request)
    {
        $ctgCliente = CtgCliente::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $ctgCliente->id]);
        }

        return redirect()->route('admin.ctg-clientes.index');

    }

    public function edit(CtgCliente $ctgCliente)
    {
        abort_if(Gate::denies('ctg_cliente_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.ctgClientes.edit', compact('ctgCliente'));
    }

    public function update(UpdateCtgClienteRequest $request, CtgCliente $ctgCliente)
    {
        $ctgCliente->update($request->all());

        return redirect()->route('admin.ctg-clientes.index');

    }

    public function show(CtgCliente $ctgCliente)
    {
        abort_if(Gate::denies('ctg_cliente_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ctgCliente->load('categoriaClientecategoria');

        return view('admin.ctgClientes.show', compact('ctgCliente'));
    }

    public function destroy(CtgCliente $ctgCliente)
    {
        abort_if(Gate::denies('ctg_cliente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ctgCliente->delete();

        return back();

    }

    public function massDestroy(MassDestroyCtgClienteRequest $request)
    {
        CtgCliente::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('ctg_cliente_create') && Gate::denies('ctg_cliente_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new CtgCliente();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
