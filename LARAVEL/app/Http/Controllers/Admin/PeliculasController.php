<?php

namespace App\Http\Controllers\Admin;

use App\Autor;
use App\Categorium;
use App\Clasificacion;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyPeliculaRequest;
use App\Http\Requests\StorePeliculaRequest;
use App\Http\Requests\UpdatePeliculaRequest;
use App\Pelicula;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class PeliculasController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('pelicula_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Pelicula::with(['autor', 'categoria', 'clasificacion'])->select(sprintf('%s.*', (new Pelicula)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'pelicula_show';
                $editGate      = 'pelicula_edit';
                $deleteGate    = 'pelicula_delete';
                $crudRoutePart = 'peliculas';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_pelicula', function ($row) {
                return $row->id_pelicula ? $row->id_pelicula : "";
            });
            $table->editColumn('nombre', function ($row) {
                return $row->nombre ? $row->nombre : "";
            });
            $table->addColumn('autor_nombre', function ($row) {
                return $row->autor ? $row->autor->nombre : '';
            });

            $table->addColumn('categoria_id_categoria', function ($row) {
                return $row->categoria ? $row->categoria->id_categoria : '';
            });

            $table->addColumn('clasificacion_id_clasificacion', function ($row) {
                return $row->clasificacion ? $row->clasificacion->id_clasificacion : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'autor', 'categoria', 'clasificacion']);

            return $table->make(true);
        }

        return view('admin.peliculas.index');
    }

    public function create()
    {
        abort_if(Gate::denies('pelicula_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $autors = Autor::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categorias = Categorium::all()->pluck('id_categoria', 'id')->prepend(trans('global.pleaseSelect'), '');

        $clasificacions = Clasificacion::all()->pluck('id_clasificacion', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.peliculas.create', compact('autors', 'categorias', 'clasificacions'));
    }

    public function store(StorePeliculaRequest $request)
    {
        $pelicula = Pelicula::create($request->all());

        return redirect()->route('admin.peliculas.index');

    }

    public function edit(Pelicula $pelicula)
    {
        abort_if(Gate::denies('pelicula_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $autors = Autor::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categorias = Categorium::all()->pluck('id_categoria', 'id')->prepend(trans('global.pleaseSelect'), '');

        $clasificacions = Clasificacion::all()->pluck('id_clasificacion', 'id')->prepend(trans('global.pleaseSelect'), '');

        $pelicula->load('autor', 'categoria', 'clasificacion');

        return view('admin.peliculas.edit', compact('autors', 'categorias', 'clasificacions', 'pelicula'));
    }

    public function update(UpdatePeliculaRequest $request, Pelicula $pelicula)
    {
        $pelicula->update($request->all());

        return redirect()->route('admin.peliculas.index');

    }

    public function show(Pelicula $pelicula)
    {
        abort_if(Gate::denies('pelicula_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pelicula->load('autor', 'categoria', 'clasificacion');

        return view('admin.peliculas.show', compact('pelicula'));
    }

    public function destroy(Pelicula $pelicula)
    {
        abort_if(Gate::denies('pelicula_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pelicula->delete();

        return back();

    }

    public function massDestroy(MassDestroyPeliculaRequest $request)
    {
        Pelicula::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
