<?php

namespace App\Http\Controllers\Admin;

use App\Clientecategorium;
use App\CtgCliente;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyClientecategoriumRequest;
use App\Http\Requests\StoreClientecategoriumRequest;
use App\Http\Requests\UpdateClientecategoriumRequest;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ClientecategoriaController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('clientecategorium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Clientecategorium::with(['cliente', 'categoria'])->select(sprintf('%s.*', (new Clientecategorium)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'clientecategorium_show';
                $editGate      = 'clientecategorium_edit';
                $deleteGate    = 'clientecategorium_delete';
                $crudRoutePart = 'clientecategoria';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_cat_user', function ($row) {
                return $row->id_cat_user ? $row->id_cat_user : "";
            });
            $table->addColumn('cliente_name', function ($row) {
                return $row->cliente ? $row->cliente->name : '';
            });

            $table->editColumn('cliente.email', function ($row) {
                return $row->cliente ? (is_string($row->cliente) ? $row->cliente : $row->cliente->email) : '';
            });
            $table->addColumn('categoria_id_categoria', function ($row) {
                return $row->categoria ? $row->categoria->id_categoria : '';
            });

            $table->editColumn('categoria.id_categoria', function ($row) {
                return $row->categoria ? (is_string($row->categoria) ? $row->categoria : $row->categoria->id_categoria) : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'cliente', 'categoria']);

            return $table->make(true);
        }

        return view('admin.clientecategoria.index');
    }

    public function create()
    {
        abort_if(Gate::denies('clientecategorium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientes = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categorias = CtgCliente::all()->pluck('id_categoria', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.clientecategoria.create', compact('clientes', 'categorias'));
    }

    public function store(StoreClientecategoriumRequest $request)
    {
        $clientecategorium = Clientecategorium::create($request->all());

        return redirect()->route('admin.clientecategoria.index');

    }

    public function edit(Clientecategorium $clientecategorium)
    {
        abort_if(Gate::denies('clientecategorium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientes = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categorias = CtgCliente::all()->pluck('id_categoria', 'id')->prepend(trans('global.pleaseSelect'), '');

        $clientecategorium->load('cliente', 'categoria');

        return view('admin.clientecategoria.edit', compact('clientes', 'categorias', 'clientecategorium'));
    }

    public function update(UpdateClientecategoriumRequest $request, Clientecategorium $clientecategorium)
    {
        $clientecategorium->update($request->all());

        return redirect()->route('admin.clientecategoria.index');

    }

    public function show(Clientecategorium $clientecategorium)
    {
        abort_if(Gate::denies('clientecategorium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientecategorium->load('cliente', 'categoria', 'clienteTickets');

        return view('admin.clientecategoria.show', compact('clientecategorium'));
    }

    public function destroy(Clientecategorium $clientecategorium)
    {
        abort_if(Gate::denies('clientecategorium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientecategorium->delete();

        return back();

    }

    public function massDestroy(MassDestroyClientecategoriumRequest $request)
    {
        Clientecategorium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
