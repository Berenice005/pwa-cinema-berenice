<?php

namespace App\Http\Controllers\Admin;

use App\Clasificacion;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyClasificacionRequest;
use App\Http\Requests\StoreClasificacionRequest;
use App\Http\Requests\UpdateClasificacionRequest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ClasificacionController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('clasificacion_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Clasificacion::query()->select(sprintf('%s.*', (new Clasificacion)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'clasificacion_show';
                $editGate      = 'clasificacion_edit';
                $deleteGate    = 'clasificacion_delete';
                $crudRoutePart = 'clasificacions';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('id_clasificacion', function ($row) {
                return $row->id_clasificacion ? $row->id_clasificacion : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.clasificacions.index');
    }

    public function create()
    {
        abort_if(Gate::denies('clasificacion_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.clasificacions.create');
    }

    public function store(StoreClasificacionRequest $request)
    {
        $clasificacion = Clasificacion::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $clasificacion->id]);
        }

        return redirect()->route('admin.clasificacions.index');

    }

    public function edit(Clasificacion $clasificacion)
    {
        abort_if(Gate::denies('clasificacion_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.clasificacions.edit', compact('clasificacion'));
    }

    public function update(UpdateClasificacionRequest $request, Clasificacion $clasificacion)
    {
        $clasificacion->update($request->all());

        return redirect()->route('admin.clasificacions.index');

    }

    public function show(Clasificacion $clasificacion)
    {
        abort_if(Gate::denies('clasificacion_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.clasificacions.show', compact('clasificacion'));
    }

    public function destroy(Clasificacion $clasificacion)
    {
        abort_if(Gate::denies('clasificacion_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clasificacion->delete();

        return back();

    }

    public function massDestroy(MassDestroyClasificacionRequest $request)
    {
        Clasificacion::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('clasificacion_create') && Gate::denies('clasificacion_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Clasificacion();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
