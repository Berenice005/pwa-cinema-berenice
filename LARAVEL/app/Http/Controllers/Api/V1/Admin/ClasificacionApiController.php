<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Clasificacion;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreClasificacionRequest;
use App\Http\Requests\UpdateClasificacionRequest;
use App\Http\Resources\Admin\ClasificacionResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ClasificacionApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('clasificacion_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ClasificacionResource(Clasificacion::all());

    }

    public function store(StoreClasificacionRequest $request)
    {
        $clasificacion = Clasificacion::create($request->all());

        return (new ClasificacionResource($clasificacion))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Clasificacion $clasificacion)
    {
        abort_if(Gate::denies('clasificacion_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ClasificacionResource($clasificacion);

    }

    public function update(UpdateClasificacionRequest $request, Clasificacion $clasificacion)
    {
        $clasificacion->update($request->all());

        return (new ClasificacionResource($clasificacion))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Clasificacion $clasificacion)
    {
        abort_if(Gate::denies('clasificacion_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clasificacion->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
