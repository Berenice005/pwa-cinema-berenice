<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Asiento;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAsientoRequest;
use App\Http\Requests\UpdateAsientoRequest;
use App\Http\Resources\Admin\AsientoResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AsientosApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('asiento_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AsientoResource(Asiento::with(['sala'])->get());

    }

    public function store(StoreAsientoRequest $request)
    {
        $asiento = Asiento::create($request->all());

        return (new AsientoResource($asiento))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Asiento $asiento)
    {
        abort_if(Gate::denies('asiento_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AsientoResource($asiento->load(['sala']));

    }

    public function update(UpdateAsientoRequest $request, Asiento $asiento)
    {
        $asiento->update($request->all());

        return (new AsientoResource($asiento))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Asiento $asiento)
    {
        abort_if(Gate::denies('asiento_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $asiento->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
