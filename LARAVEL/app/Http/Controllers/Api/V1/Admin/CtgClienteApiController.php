<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\CtgCliente;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreCtgClienteRequest;
use App\Http\Requests\UpdateCtgClienteRequest;
use App\Http\Resources\Admin\CtgClienteResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CtgClienteApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('ctg_cliente_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CtgClienteResource(CtgCliente::all());

    }

    public function store(StoreCtgClienteRequest $request)
    {
        $ctgCliente = CtgCliente::create($request->all());

        return (new CtgClienteResource($ctgCliente))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(CtgCliente $ctgCliente)
    {
        abort_if(Gate::denies('ctg_cliente_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CtgClienteResource($ctgCliente);

    }

    public function update(UpdateCtgClienteRequest $request, CtgCliente $ctgCliente)
    {
        $ctgCliente->update($request->all());

        return (new CtgClienteResource($ctgCliente))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(CtgCliente $ctgCliente)
    {
        abort_if(Gate::denies('ctg_cliente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ctgCliente->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
