<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Clientecategorium;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClientecategoriumRequest;
use App\Http\Requests\UpdateClientecategoriumRequest;
use App\Http\Resources\Admin\ClientecategoriumResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientecategoriaApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('clientecategorium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ClientecategoriumResource(Clientecategorium::with(['cliente', 'categoria'])->get());

    }

    public function store(StoreClientecategoriumRequest $request)
    {
        $clientecategorium = Clientecategorium::create($request->all());

        return (new ClientecategoriumResource($clientecategorium))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Clientecategorium $clientecategorium)
    {
        abort_if(Gate::denies('clientecategorium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ClientecategoriumResource($clientecategorium->load(['cliente', 'categoria']));

    }

    public function update(UpdateClientecategoriumRequest $request, Clientecategorium $clientecategorium)
    {
        $clientecategorium->update($request->all());

        return (new ClientecategoriumResource($clientecategorium))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Clientecategorium $clientecategorium)
    {
        abort_if(Gate::denies('clientecategorium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clientecategorium->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
