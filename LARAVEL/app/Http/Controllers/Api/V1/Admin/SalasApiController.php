<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSalaRequest;
use App\Http\Requests\UpdateSalaRequest;
use App\Http\Resources\Admin\SalaResource;
use App\Sala;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SalasApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('sala_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SalaResource(Sala::all());

    }

    public function store(StoreSalaRequest $request)
    {
        $sala = Sala::create($request->all());

        return (new SalaResource($sala))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Sala $sala)
    {
        abort_if(Gate::denies('sala_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SalaResource($sala);

    }

    public function update(UpdateSalaRequest $request, Sala $sala)
    {
        $sala->update($request->all());

        return (new SalaResource($sala))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Sala $sala)
    {
        abort_if(Gate::denies('sala_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sala->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
