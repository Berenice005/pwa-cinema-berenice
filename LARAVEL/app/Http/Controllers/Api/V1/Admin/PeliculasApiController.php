<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePeliculaRequest;
use App\Http\Requests\UpdatePeliculaRequest;
use App\Http\Resources\Admin\PeliculaResource;
use App\Pelicula;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PeliculasApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('pelicula_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PeliculaResource(Pelicula::with(['autor', 'categoria', 'clasificacion'])->get());

    }

    public function store(StorePeliculaRequest $request)
    {
        $pelicula = Pelicula::create($request->all());

        return (new PeliculaResource($pelicula))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Pelicula $pelicula)
    {
        abort_if(Gate::denies('pelicula_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PeliculaResource($pelicula->load(['autor', 'categoria', 'clasificacion']));

    }

    public function update(UpdatePeliculaRequest $request, Pelicula $pelicula)
    {
        $pelicula->update($request->all());

        return (new PeliculaResource($pelicula))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Pelicula $pelicula)
    {
        abort_if(Gate::denies('pelicula_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pelicula->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
