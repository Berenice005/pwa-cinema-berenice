<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Boleto;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBoletoRequest;
use App\Http\Requests\UpdateBoletoRequest;
use App\Http\Resources\Admin\BoletoResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BoletosApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('boleto_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BoletoResource(Boleto::with(['pelicula'])->get());

    }

    public function store(StoreBoletoRequest $request)
    {
        $boleto = Boleto::create($request->all());

        return (new BoletoResource($boleto))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Boleto $boleto)
    {
        abort_if(Gate::denies('boleto_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BoletoResource($boleto->load(['pelicula']));

    }

    public function update(UpdateBoletoRequest $request, Boleto $boleto)
    {
        $boleto->update($request->all());

        return (new BoletoResource($boleto))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Boleto $boleto)
    {
        abort_if(Gate::denies('boleto_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $boleto->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
