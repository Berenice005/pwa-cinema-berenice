<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Boleto extends Model
{
    use SoftDeletes;

    public $table = 'boletos';

    protected $dates = [
        'horario',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'precio',
        'horario',
        'id_boleto',
        'created_at',
        'updated_at',
        'deleted_at',
        'pelicula_id',
    ];

    public function idBoletoTickets()
    {
        return $this->hasMany(Ticket::class, 'id_boleto_id', 'id');

    }

    public function pelicula()
    {
        return $this->belongsTo(Pelicula::class, 'pelicula_id');

    }

    public function getHorarioAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;

    }

    public function setHorarioAttribute($value)
    {
        $this->attributes['horario'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;

    }
}
