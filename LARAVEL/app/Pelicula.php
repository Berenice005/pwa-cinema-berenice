<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pelicula extends Model
{
    use SoftDeletes;

    public $table = 'peliculas';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'nombre',
        'autor_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'id_pelicula',
        'categoria_id',
        'clasificacion_id',
    ];

    public function autor()
    {
        return $this->belongsTo(Autor::class, 'autor_id');

    }

    public function categoria()
    {
        return $this->belongsTo(Categorium::class, 'categoria_id');

    }

    public function clasificacion()
    {
        return $this->belongsTo(Clasificacion::class, 'clasificacion_id');

    }
}
