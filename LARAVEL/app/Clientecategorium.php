<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clientecategorium extends Model
{
    use SoftDeletes;

    public $table = 'clientecategoria';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'cliente_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'id_cat_user',
        'categoria_id',
    ];

    public function clienteTickets()
    {
        return $this->hasMany(Ticket::class, 'cliente_id', 'id');

    }

    public function cliente()
    {
        return $this->belongsTo(User::class, 'cliente_id');

    }

    public function categoria()
    {
        return $this->belongsTo(Clientecategorium::class, 'categoria_id');

    }
}
