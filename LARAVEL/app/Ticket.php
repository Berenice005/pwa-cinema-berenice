<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    public $table = 'tickets';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id_ticket',
        'cliente_id',
        'asiento_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'id_boleto_id',
    ];

    public function cliente()
    {
        return $this->belongsTo(Clientecategorium::class, 'cliente_id');

    }

    public function id_boleto()
    {
        return $this->belongsTo(Boleto::class, 'id_boleto_id');

    }

    public function asiento()
    {
        return $this->belongsTo(Asiento::class, 'asiento_id');

    }
}
