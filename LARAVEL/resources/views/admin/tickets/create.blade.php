@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.ticket.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.tickets.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('id_ticket') ? 'has-error' : '' }}">
                            <label class="required" for="id_ticket">{{ trans('cruds.ticket.fields.id_ticket') }}</label>
                            <input class="form-control" type="text" name="id_ticket" id="id_ticket" value="{{ old('id_ticket', '') }}" required>
                            @if($errors->has('id_ticket'))
                                <span class="help-block" role="alert">{{ $errors->first('id_ticket') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.ticket.fields.id_ticket_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('cliente') ? 'has-error' : '' }}">
                            <label class="required" for="cliente_id">{{ trans('cruds.ticket.fields.cliente') }}</label>
                            <select class="form-control select2" name="cliente_id" id="cliente_id" required>
                                @foreach($clientes as $id => $cliente)
                                    <option value="{{ $id }}" {{ old('cliente_id') == $id ? 'selected' : '' }}>{{ $cliente }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('cliente'))
                                <span class="help-block" role="alert">{{ $errors->first('cliente') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.ticket.fields.cliente_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('id_boleto') ? 'has-error' : '' }}">
                            <label class="required" for="id_boleto_id">{{ trans('cruds.ticket.fields.id_boleto') }}</label>
                            <select class="form-control select2" name="id_boleto_id" id="id_boleto_id" required>
                                @foreach($id_boletos as $id => $id_boleto)
                                    <option value="{{ $id }}" {{ old('id_boleto_id') == $id ? 'selected' : '' }}>{{ $id_boleto }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('id_boleto'))
                                <span class="help-block" role="alert">{{ $errors->first('id_boleto') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.ticket.fields.id_boleto_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('asiento') ? 'has-error' : '' }}">
                            <label class="required" for="asiento_id">{{ trans('cruds.ticket.fields.asiento') }}</label>
                            <select class="form-control select2" name="asiento_id" id="asiento_id" required>
                                @foreach($asientos as $id => $asiento)
                                    <option value="{{ $id }}" {{ old('asiento_id') == $id ? 'selected' : '' }}>{{ $asiento }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('asiento'))
                                <span class="help-block" role="alert">{{ $errors->first('asiento') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.ticket.fields.asiento_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection