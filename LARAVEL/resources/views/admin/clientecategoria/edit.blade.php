@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.clientecategorium.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.clientecategoria.update", [$clientecategorium->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('id_cat_user') ? 'has-error' : '' }}">
                            <label class="required" for="id_cat_user">{{ trans('cruds.clientecategorium.fields.id_cat_user') }}</label>
                            <input class="form-control" type="text" name="id_cat_user" id="id_cat_user" value="{{ old('id_cat_user', $clientecategorium->id_cat_user) }}" required>
                            @if($errors->has('id_cat_user'))
                                <span class="help-block" role="alert">{{ $errors->first('id_cat_user') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.clientecategorium.fields.id_cat_user_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('cliente') ? 'has-error' : '' }}">
                            <label class="required" for="cliente_id">{{ trans('cruds.clientecategorium.fields.cliente') }}</label>
                            <select class="form-control select2" name="cliente_id" id="cliente_id" required>
                                @foreach($clientes as $id => $cliente)
                                    <option value="{{ $id }}" {{ ($clientecategorium->cliente ? $clientecategorium->cliente->id : old('cliente_id')) == $id ? 'selected' : '' }}>{{ $cliente }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('cliente'))
                                <span class="help-block" role="alert">{{ $errors->first('cliente') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.clientecategorium.fields.cliente_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
                            <label class="required" for="categoria_id">{{ trans('cruds.clientecategorium.fields.categoria') }}</label>
                            <select class="form-control select2" name="categoria_id" id="categoria_id" required>
                                @foreach($categorias as $id => $categoria)
                                    <option value="{{ $id }}" {{ ($clientecategorium->categoria ? $clientecategorium->categoria->id : old('categoria_id')) == $id ? 'selected' : '' }}>{{ $categoria }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('categoria'))
                                <span class="help-block" role="alert">{{ $errors->first('categoria') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.clientecategorium.fields.categoria_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection