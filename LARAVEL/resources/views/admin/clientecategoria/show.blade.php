@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.clientecategorium.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.clientecategoria.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $clientecategorium->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.id_cat_user') }}
                                    </th>
                                    <td>
                                        {{ $clientecategorium->id_cat_user }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.cliente') }}
                                    </th>
                                    <td>
                                        {{ $clientecategorium->cliente->name ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.categoria') }}
                                    </th>
                                    <td>
                                        {{ $clientecategorium->categoria->id_categoria ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.clientecategoria.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#cliente_tickets" aria-controls="cliente_tickets" role="tab" data-toggle="tab">
                            {{ trans('cruds.ticket.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="cliente_tickets">
                        @includeIf('admin.clientecategoria.relationships.clienteTickets', ['tickets' => $clientecategorium->clienteTickets])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection