@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.boleto.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.boletos.update", [$boleto->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('id_boleto') ? 'has-error' : '' }}">
                            <label class="required" for="id_boleto">{{ trans('cruds.boleto.fields.id_boleto') }}</label>
                            <input class="form-control" type="text" name="id_boleto" id="id_boleto" value="{{ old('id_boleto', $boleto->id_boleto) }}" required>
                            @if($errors->has('id_boleto'))
                                <span class="help-block" role="alert">{{ $errors->first('id_boleto') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.boleto.fields.id_boleto_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('pelicula') ? 'has-error' : '' }}">
                            <label class="required" for="pelicula_id">{{ trans('cruds.boleto.fields.pelicula') }}</label>
                            <select class="form-control select2" name="pelicula_id" id="pelicula_id" required>
                                @foreach($peliculas as $id => $pelicula)
                                    <option value="{{ $id }}" {{ ($boleto->pelicula ? $boleto->pelicula->id : old('pelicula_id')) == $id ? 'selected' : '' }}>{{ $pelicula }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('pelicula'))
                                <span class="help-block" role="alert">{{ $errors->first('pelicula') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.boleto.fields.pelicula_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('precio') ? 'has-error' : '' }}">
                            <label class="required" for="precio">{{ trans('cruds.boleto.fields.precio') }}</label>
                            <input class="form-control" type="number" name="precio" id="precio" value="{{ old('precio', $boleto->precio) }}" step="0.01" required>
                            @if($errors->has('precio'))
                                <span class="help-block" role="alert">{{ $errors->first('precio') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.boleto.fields.precio_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('horario') ? 'has-error' : '' }}">
                            <label class="required" for="horario">{{ trans('cruds.boleto.fields.horario') }}</label>
                            <input class="form-control datetime" type="text" name="horario" id="horario" value="{{ old('horario', $boleto->horario) }}" required>
                            @if($errors->has('horario'))
                                <span class="help-block" role="alert">{{ $errors->first('horario') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.boleto.fields.horario_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection