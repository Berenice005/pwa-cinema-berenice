@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.boleto.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.boletos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.boleto.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $boleto->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.boleto.fields.id_boleto') }}
                                    </th>
                                    <td>
                                        {{ $boleto->id_boleto }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.boleto.fields.pelicula') }}
                                    </th>
                                    <td>
                                        {{ $boleto->pelicula->nombre ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.boleto.fields.precio') }}
                                    </th>
                                    <td>
                                        {{ $boleto->precio }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.boleto.fields.horario') }}
                                    </th>
                                    <td>
                                        {{ $boleto->horario }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.boletos.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#id_boleto_tickets" aria-controls="id_boleto_tickets" role="tab" data-toggle="tab">
                            {{ trans('cruds.ticket.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="id_boleto_tickets">
                        @includeIf('admin.boletos.relationships.idBoletoTickets', ['tickets' => $boleto->idBoletoTickets])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection