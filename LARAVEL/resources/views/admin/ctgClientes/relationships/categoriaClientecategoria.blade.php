<div class="content">
    @can('clientecategorium_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.clientecategoria.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.clientecategorium.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.clientecategorium.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Clientecategorium">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.id_cat_user') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.cliente') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.email') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.clientecategorium.fields.categoria') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.ctgCliente.fields.id_categoria') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($clientecategoria as $key => $clientecategorium)
                                    <tr data-entry-id="{{ $clientecategorium->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $clientecategorium->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $clientecategorium->id_cat_user ?? '' }}
                                        </td>
                                        <td>
                                            {{ $clientecategorium->cliente->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $clientecategorium->cliente->email ?? '' }}
                                        </td>
                                        <td>
                                            {{ $clientecategorium->categoria->id_categoria ?? '' }}
                                        </td>
                                        <td>
                                            {{ $clientecategorium->categoria->id_categoria ?? '' }}
                                        </td>
                                        <td>
                                            @can('clientecategorium_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.clientecategoria.show', $clientecategorium->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('clientecategorium_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.clientecategoria.edit', $clientecategorium->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('clientecategorium_delete')
                                                <form action="{{ route('admin.clientecategoria.destroy', $clientecategorium->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('clientecategorium_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.clientecategoria.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Clientecategorium:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection