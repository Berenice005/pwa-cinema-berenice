@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.ctgCliente.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.ctg-clientes.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.ctgCliente.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $ctgCliente->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.ctgCliente.fields.id_categoria') }}
                                    </th>
                                    <td>
                                        {{ $ctgCliente->id_categoria }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.ctgCliente.fields.descripcion') }}
                                    </th>
                                    <td>
                                        {!! $ctgCliente->descripcion !!}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.ctg-clientes.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.relatedData') }}
                </div>
                <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
                    <li role="presentation">
                        <a href="#categoria_clientecategoria" aria-controls="categoria_clientecategoria" role="tab" data-toggle="tab">
                            {{ trans('cruds.clientecategorium.title') }}
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" role="tabpanel" id="categoria_clientecategoria">
                        @includeIf('admin.ctgClientes.relationships.categoriaClientecategoria', ['clientecategoria' => $ctgCliente->categoriaClientecategoria])
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection