@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.pelicula.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.peliculas.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('id_pelicula') ? 'has-error' : '' }}">
                            <label class="required" for="id_pelicula">{{ trans('cruds.pelicula.fields.id_pelicula') }}</label>
                            <input class="form-control" type="text" name="id_pelicula" id="id_pelicula" value="{{ old('id_pelicula', '') }}" required>
                            @if($errors->has('id_pelicula'))
                                <span class="help-block" role="alert">{{ $errors->first('id_pelicula') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pelicula.fields.id_pelicula_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.pelicula.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', '') }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pelicula.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('autor') ? 'has-error' : '' }}">
                            <label class="required" for="autor_id">{{ trans('cruds.pelicula.fields.autor') }}</label>
                            <select class="form-control select2" name="autor_id" id="autor_id" required>
                                @foreach($autors as $id => $autor)
                                    <option value="{{ $id }}" {{ old('autor_id') == $id ? 'selected' : '' }}>{{ $autor }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('autor'))
                                <span class="help-block" role="alert">{{ $errors->first('autor') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pelicula.fields.autor_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
                            <label class="required" for="categoria_id">{{ trans('cruds.pelicula.fields.categoria') }}</label>
                            <select class="form-control select2" name="categoria_id" id="categoria_id" required>
                                @foreach($categorias as $id => $categoria)
                                    <option value="{{ $id }}" {{ old('categoria_id') == $id ? 'selected' : '' }}>{{ $categoria }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('categoria'))
                                <span class="help-block" role="alert">{{ $errors->first('categoria') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pelicula.fields.categoria_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('clasificacion') ? 'has-error' : '' }}">
                            <label class="required" for="clasificacion_id">{{ trans('cruds.pelicula.fields.clasificacion') }}</label>
                            <select class="form-control select2" name="clasificacion_id" id="clasificacion_id" required>
                                @foreach($clasificacions as $id => $clasificacion)
                                    <option value="{{ $id }}" {{ old('clasificacion_id') == $id ? 'selected' : '' }}>{{ $clasificacion }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('clasificacion'))
                                <span class="help-block" role="alert">{{ $errors->first('clasificacion') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pelicula.fields.clasificacion_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection