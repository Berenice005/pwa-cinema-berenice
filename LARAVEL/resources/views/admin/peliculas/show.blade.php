@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.pelicula.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.peliculas.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.pelicula.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $pelicula->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.pelicula.fields.id_pelicula') }}
                                    </th>
                                    <td>
                                        {{ $pelicula->id_pelicula }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.pelicula.fields.nombre') }}
                                    </th>
                                    <td>
                                        {{ $pelicula->nombre }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.pelicula.fields.autor') }}
                                    </th>
                                    <td>
                                        {{ $pelicula->autor->nombre ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.pelicula.fields.categoria') }}
                                    </th>
                                    <td>
                                        {{ $pelicula->categoria->id_categoria ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.pelicula.fields.clasificacion') }}
                                    </th>
                                    <td>
                                        {{ $pelicula->clasificacion->id_clasificacion ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.peliculas.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection