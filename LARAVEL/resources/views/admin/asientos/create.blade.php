<head>
@laravelPWA
</head>
@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.asiento.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.asientos.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('id_asiento') ? 'has-error' : '' }}">
                            <label class="required" for="id_asiento">{{ trans('cruds.asiento.fields.id_asiento') }}</label>
                            <input class="form-control" type="text" name="id_asiento" id="id_asiento" value="{{ old('id_asiento', '') }}" required>
                            @if($errors->has('id_asiento'))
                                <span class="help-block" role="alert">{{ $errors->first('id_asiento') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.asiento.fields.id_asiento_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('sala') ? 'has-error' : '' }}">
                            <label class="required" for="sala_id">{{ trans('cruds.asiento.fields.sala') }}</label>
                            <select class="form-control select2" name="sala_id" id="sala_id" required>
                                @foreach($salas as $id => $sala)
                                    <option value="{{ $id }}" {{ old('sala_id') == $id ? 'selected' : '' }}>{{ $sala }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('sala'))
                                <span class="help-block" role="alert">{{ $errors->first('sala') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.asiento.fields.sala_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection