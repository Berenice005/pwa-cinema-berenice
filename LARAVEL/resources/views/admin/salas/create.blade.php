@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.sala.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.salas.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('id_sala') ? 'has-error' : '' }}">
                            <label class="required" for="id_sala">{{ trans('cruds.sala.fields.id_sala') }}</label>
                            <input class="form-control" type="text" name="id_sala" id="id_sala" value="{{ old('id_sala', '') }}" required>
                            @if($errors->has('id_sala'))
                                <span class="help-block" role="alert">{{ $errors->first('id_sala') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.sala.fields.id_sala_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection