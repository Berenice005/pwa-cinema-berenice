<div class="content">
    @can('compago_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.compagos.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.compago.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.compago.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Compago">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        {{ trans('cruds.compago.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.compago.fields.alumno') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user.fields.email') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.compago.fields.monto') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.compago.fields.carrera') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.compago.fields.comprobante') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.compago.fields.folio') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($compagos as $key => $compago)
                                    <tr data-entry-id="{{ $compago->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $compago->id ?? '' }}
                                        </td>
                                        <td>
                                            {{ $compago->alumno->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $compago->alumno->email ?? '' }}
                                        </td>
                                        <td>
                                            {{ $compago->monto ?? '' }}
                                        </td>
                                        <td>
                                            {{ $compago->carrera->nombrecarrera ?? '' }}
                                        </td>
                                        <td>
                                            @if($compago->comprobante)
                                                <a href="{{ $compago->comprobante->getUrl() }}" target="_blank">
                                                    {{ trans('global.view_file') }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $compago->folio ?? '' }}
                                        </td>
                                        <td>
                                            @can('compago_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.compagos.show', $compago->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('compago_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.compagos.edit', $compago->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('compago_delete')
                                                <form action="{{ route('admin.compagos.destroy', $compago->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('compago_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.compagos.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Compago:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection