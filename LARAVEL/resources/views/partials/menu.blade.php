<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu tree" data-widget="tree">
            <li>
                <a href="{{ route("admin.home") }}">
                    <i class="fas fa-fw fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            @can('user_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa-fw fas fa-users">

                        </i>
                        <span>{{ trans('cruds.userManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('permission_access')
                            <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.permissions.index") }}">
                                    <i class="fa-fw fas fa-unlock-alt">

                                    </i>
                                    <span>{{ trans('cruds.permission.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('role_access')
                            <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.roles.index") }}">
                                    <i class="fa-fw fas fa-briefcase">

                                    </i>
                                    <span>{{ trans('cruds.role.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('user_access')
                            <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.users.index") }}">
                                    <i class="fa-fw fas fa-user">

                                    </i>
                                    <span>{{ trans('cruds.user.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('team_access')
                            <li class="{{ request()->is('admin/teams') || request()->is('admin/teams/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.teams.index") }}">
                                    <i class="fa-fw fas fa-users">

                                    </i>
                                    <span>{{ trans('cruds.team.title') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('categorium_access')
                <li class="{{ request()->is('admin/categoria') || request()->is('admin/categoria/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.categoria.index") }}">
                        <i class="fa-fw fas fa-bars">

                        </i>
                        <span>{{ trans('cruds.categorium.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('clasificacion_access')
                <li class="{{ request()->is('admin/clasificacions') || request()->is('admin/clasificacions/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.clasificacions.index") }}">
                        <i class="fa-fw far fa-clone">

                        </i>
                        <span>{{ trans('cruds.clasificacion.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('autor_access')
                <li class="{{ request()->is('admin/autors') || request()->is('admin/autors/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.autors.index") }}">
                        <i class="fa-fw fas fa-user-alt">

                        </i>
                        <span>{{ trans('cruds.autor.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('pelicula_access')
                <li class="{{ request()->is('admin/peliculas') || request()->is('admin/peliculas/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.peliculas.index") }}">
                        <i class="fa-fw fas fa-film">

                        </i>
                        <span>{{ trans('cruds.pelicula.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('boleto_access')
                <li class="{{ request()->is('admin/boletos') || request()->is('admin/boletos/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.boletos.index") }}">
                        <i class="fa-fw fas fa-ticket-alt">

                        </i>
                        <span>{{ trans('cruds.boleto.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('ctg_cliente_access')
                <li class="{{ request()->is('admin/ctg-clientes') || request()->is('admin/ctg-clientes/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.ctg-clientes.index") }}">
                        <i class="fa-fw fas fa-star">

                        </i>
                        <span>{{ trans('cruds.ctgCliente.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('clientecategorium_access')
                <li class="{{ request()->is('admin/clientecategoria') || request()->is('admin/clientecategoria/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.clientecategoria.index") }}">
                        <i class="fa-fw fas fa-diagnoses">

                        </i>
                        <span>{{ trans('cruds.clientecategorium.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('asiento_access')
                <li class="{{ request()->is('admin/asientos') || request()->is('admin/asientos/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.asientos.index") }}">
                        <i class="fa-fw fas fa-list-ol">

                        </i>
                        <span>{{ trans('cruds.asiento.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('sala_access')
                <li class="{{ request()->is('admin/salas') || request()->is('admin/salas/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.salas.index") }}">
                        <i class="fa-fw fas fa-store-alt">

                        </i>
                        <span>{{ trans('cruds.sala.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('ticket_access')
                <li class="{{ request()->is('admin/tickets') || request()->is('admin/tickets/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.tickets.index") }}">
                        <i class="fa-fw fas fa-shopping-cart">

                        </i>
                        <span>{{ trans('cruds.ticket.title') }}</span>
                    </a>
                </li>
            @endcan
            <li class="{{ request()->is('admin/system-calendar') || request()->is('admin/system-calendar/*') ? 'active' : '' }}">
                <a href="{{ route("admin.systemCalendar") }}">
                    <i class="fas fa-fw fa-calendar">

                    </i>
                    <span>{{ trans('global.systemCalendar') }}</span>
                </a>
            </li>
            @php($unread = \App\QaTopic::unreadCount())
                <li class="{{ request()->is('admin/messenger') || request()->is('admin/messenger/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.messenger.index") }}">
                        <i class="fa-fw fa fa-envelope">

                        </i>
                        <span>{{ trans('global.messages') }}</span>
                        @if($unread > 0)
                            <strong>( {{ $unread }} )</strong>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="fas fa-fw fa-sign-out-alt">

                        </i>
                        {{ trans('global.logout') }}
                    </a>
                </li>
        </ul>
    </section>
</aside>