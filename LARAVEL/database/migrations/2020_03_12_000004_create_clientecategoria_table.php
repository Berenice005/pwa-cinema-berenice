<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientecategoriaTable extends Migration
{
    public function up()
    {
        Schema::create('clientecategoria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_cat_user')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
