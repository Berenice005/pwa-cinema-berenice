<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToBoletosTable extends Migration
{
    public function up()
    {
        Schema::table('boletos', function (Blueprint $table) {
            $table->unsignedInteger('pelicula_id');
            $table->foreign('pelicula_id', 'pelicula_fk_1135165')->references('id')->on('peliculas');
        });

    }
}
