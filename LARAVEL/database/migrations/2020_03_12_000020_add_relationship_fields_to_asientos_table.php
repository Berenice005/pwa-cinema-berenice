<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAsientosTable extends Migration
{
    public function up()
    {
        Schema::table('asientos', function (Blueprint $table) {
            $table->unsignedInteger('sala_id');
            $table->foreign('sala_id', 'sala_fk_1135276')->references('id')->on('salas');
        });

    }
}
