<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPeliculasTable extends Migration
{
    public function up()
    {
        Schema::table('peliculas', function (Blueprint $table) {
            $table->unsignedInteger('autor_id');
            $table->foreign('autor_id', 'autor_fk_1135151')->references('id')->on('autors');
            $table->unsignedInteger('categoria_id');
            $table->foreign('categoria_id', 'categoria_fk_1135161')->references('id')->on('categoria');
            $table->unsignedInteger('clasificacion_id');
            $table->foreign('clasificacion_id', 'clasificacion_fk_1135162')->references('id')->on('clasificacions');
        });

    }
}
