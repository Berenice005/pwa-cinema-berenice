<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTicketsTable extends Migration
{
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedInteger('cliente_id');
            $table->foreign('cliente_id', 'cliente_fk_1135324')->references('id')->on('clientecategoria');
            $table->unsignedInteger('id_boleto_id');
            $table->foreign('id_boleto_id', 'id_boleto_fk_1135325')->references('id')->on('boletos');
            $table->unsignedInteger('asiento_id');
            $table->foreign('asiento_id', 'asiento_fk_1135326')->references('id')->on('asientos');
        });

    }
}
