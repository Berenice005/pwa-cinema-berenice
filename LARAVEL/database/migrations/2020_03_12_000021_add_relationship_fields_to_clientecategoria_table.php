<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToClientecategoriaTable extends Migration
{
    public function up()
    {
        Schema::table('clientecategoria', function (Blueprint $table) {
            $table->unsignedInteger('cliente_id');
            $table->foreign('cliente_id', 'cliente_fk_1135218')->references('id')->on('users');
            $table->unsignedInteger('categoria_id');
            $table->foreign('categoria_id', 'categoria_fk_1135222')->references('id')->on('ctg_clientes');
        });

    }
}
