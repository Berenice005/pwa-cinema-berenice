<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletosTable extends Migration
{
    public function up()
    {
        Schema::create('boletos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_boleto')->unique();
            $table->decimal('precio', 15, 2);
            $table->datetime('horario');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
