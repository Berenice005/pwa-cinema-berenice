<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'categorium_create',
            ],
            [
                'id'    => '18',
                'title' => 'categorium_edit',
            ],
            [
                'id'    => '19',
                'title' => 'categorium_show',
            ],
            [
                'id'    => '20',
                'title' => 'categorium_delete',
            ],
            [
                'id'    => '21',
                'title' => 'categorium_access',
            ],
            [
                'id'    => '22',
                'title' => 'clasificacion_create',
            ],
            [
                'id'    => '23',
                'title' => 'clasificacion_edit',
            ],
            [
                'id'    => '24',
                'title' => 'clasificacion_show',
            ],
            [
                'id'    => '25',
                'title' => 'clasificacion_delete',
            ],
            [
                'id'    => '26',
                'title' => 'clasificacion_access',
            ],
            [
                'id'    => '27',
                'title' => 'autor_create',
            ],
            [
                'id'    => '28',
                'title' => 'autor_edit',
            ],
            [
                'id'    => '29',
                'title' => 'autor_show',
            ],
            [
                'id'    => '30',
                'title' => 'autor_delete',
            ],
            [
                'id'    => '31',
                'title' => 'autor_access',
            ],
            [
                'id'    => '32',
                'title' => 'pelicula_create',
            ],
            [
                'id'    => '33',
                'title' => 'pelicula_edit',
            ],
            [
                'id'    => '34',
                'title' => 'pelicula_show',
            ],
            [
                'id'    => '35',
                'title' => 'pelicula_delete',
            ],
            [
                'id'    => '36',
                'title' => 'pelicula_access',
            ],
            [
                'id'    => '37',
                'title' => 'boleto_create',
            ],
            [
                'id'    => '38',
                'title' => 'boleto_edit',
            ],
            [
                'id'    => '39',
                'title' => 'boleto_show',
            ],
            [
                'id'    => '40',
                'title' => 'boleto_delete',
            ],
            [
                'id'    => '41',
                'title' => 'boleto_access',
            ],
            [
                'id'    => '42',
                'title' => 'ctg_cliente_create',
            ],
            [
                'id'    => '43',
                'title' => 'ctg_cliente_edit',
            ],
            [
                'id'    => '44',
                'title' => 'ctg_cliente_show',
            ],
            [
                'id'    => '45',
                'title' => 'ctg_cliente_delete',
            ],
            [
                'id'    => '46',
                'title' => 'ctg_cliente_access',
            ],
            [
                'id'    => '47',
                'title' => 'clientecategorium_create',
            ],
            [
                'id'    => '48',
                'title' => 'clientecategorium_edit',
            ],
            [
                'id'    => '49',
                'title' => 'clientecategorium_show',
            ],
            [
                'id'    => '50',
                'title' => 'clientecategorium_delete',
            ],
            [
                'id'    => '51',
                'title' => 'clientecategorium_access',
            ],
            [
                'id'    => '52',
                'title' => 'asiento_create',
            ],
            [
                'id'    => '53',
                'title' => 'asiento_edit',
            ],
            [
                'id'    => '54',
                'title' => 'asiento_show',
            ],
            [
                'id'    => '55',
                'title' => 'asiento_delete',
            ],
            [
                'id'    => '56',
                'title' => 'asiento_access',
            ],
            [
                'id'    => '57',
                'title' => 'sala_create',
            ],
            [
                'id'    => '58',
                'title' => 'sala_edit',
            ],
            [
                'id'    => '59',
                'title' => 'sala_show',
            ],
            [
                'id'    => '60',
                'title' => 'sala_delete',
            ],
            [
                'id'    => '61',
                'title' => 'sala_access',
            ],
            [
                'id'    => '62',
                'title' => 'ticket_create',
            ],
            [
                'id'    => '63',
                'title' => 'ticket_edit',
            ],
            [
                'id'    => '64',
                'title' => 'ticket_show',
            ],
            [
                'id'    => '65',
                'title' => 'ticket_delete',
            ],
            [
                'id'    => '66',
                'title' => 'ticket_access',
            ],
            [
                'id'    => '67',
                'title' => 'team_create',
            ],
            [
                'id'    => '68',
                'title' => 'team_edit',
            ],
            [
                'id'    => '69',
                'title' => 'team_show',
            ],
            [
                'id'    => '70',
                'title' => 'team_delete',
            ],
            [
                'id'    => '71',
                'title' => 'team_access',
            ],
        ];

        Permission::insert($permissions);

    }
}
