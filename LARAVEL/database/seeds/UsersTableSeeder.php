<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$hv9XGeGZb/p/8jNMUdt6o.noWBP0CBVoni1vFW4y/y2uIKYbUxfeS',
                'remember_token' => null,
            ],
        ];

        User::insert($users);

    }
}
