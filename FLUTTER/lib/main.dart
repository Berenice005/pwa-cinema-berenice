import 'package:flutter/material.dart'; //unico import de material desing

void main() => runApp(MyApp()); //correr

class MyApp extends StatelessWidget {

  Container misPelis (String imageVal, String head, String subhead){ //clase pelis
    return Container(
                width:160.0,
                child: Card(
                  child: Wrap(
                  children: <Widget>[
                  Image.network(imageVal),
                  ListTile(
                    title: Text(head),
                    subtitle: Text(subhead),
                  ),
                  ],
                ),
             ), 
       );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'PELIS EN FLUTTER DART',
      color: Colors.blue,
      theme: ThemeData(
       //primaryColor: Colors.black,//Colors.blue new Color (0xff622F74)
      ),
     home:Scaffold(
       backgroundColor: Colors.black,
       appBar: AppBar(
         title: Text("Pelis en Dart",style: TextStyle(color: Colors.black), ),
      backgroundColor: Colors.white,
         ),
         body:Container(
           margin: EdgeInsets.symmetric(vertical:20.0),
          height: 300,
          color: Colors.black,
          child:ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              misPelis("https://images.unsplash.com/photo-1503875154399-95d2b151e3b0?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "Peli 1", "Peli 1 es buena"),
              misPelis("https://images.unsplash.com/photo-1484581400079-58a319a15a2a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjIxMTIzfQ&auto=format&fit=crop&w=500&q=60", "Peli 2", "Peli 2 es buena"),
              misPelis("https://images.unsplash.com/photo-1503875154399-95d2b151e3b0?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60", "Peli 1", "Peli 1 es buena"),
              misPelis("https://images.unsplash.com/photo-1484581400079-58a319a15a2a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjIxMTIzfQ&auto=format&fit=crop&w=500&q=60", "Peli 2", "Peli 2 es buena"),
            ],
          ),
         ),
     ),
    );
  }
}

